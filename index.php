
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Challenge Linio!</title>
  </head>
  <body>
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                 <h1>Challenge Linio</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        <button type="button" class="btn btn-secondary" id="propuesta1" >1</button>
                        <button type="button" class="btn btn-secondary" id="propuesta2">2</button>
                        <button type="button" class="btn btn-secondary" id="propuesta3">3</button>
                    </div>
                </div>
        </div>

        <div class="row">
            <div class="col-sm-12" id="result">
            
            </div>
        </div>
    </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="libreria/jquery.js">
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script>
        $( document ).ready(function() {
            $( "#propuesta1" ).on( "click", function() {
                   llamadaServer(1);
            });

            $( "#propuesta2" ).on( "click", function() {
                   llamadaServer(2);
            });

            $( "#propuesta3" ).on( "click", function() {
                   llamadaServer(3);
            });

            function llamadaServer(propuesta){

                $.ajax({
                    data:   { propuesta: propuesta },
                    url:   "InvocacionesPropuestas.php", 
                    type:  'post', 
                    beforeSend: function () {
                            $("#result").html("Procesando, espere por favor...");
                    },
                    success:  function (response) { 
                        $( "#result" ).html( response );
                        }
                    });
                }
        });

       

    </script>
  
  </body>
</html>







