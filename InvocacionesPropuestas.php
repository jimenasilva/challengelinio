<?php

class InvocacionesPropuestas{

    private $html;

    private function armarEncabezado($propuesta){
        $this->html ='<table class="table">
        <thead>
        <tr>
            <th scope="col">Propuesta: '.$propuesta.'</th>
          </tr>
          <tr>
            <th scope="col">Index</th>
            <th scope="col">Resultado</th>
          </tr>
        </thead>';
    }

    function invocar($accion){
        $this->armarEncabezado($accion);

        switch ($accion) {
            case 1:
                $this->invocarPropuesta1();
                echo  $this->html;
              break;
              case 2:
                $this->invocarPropuesta2();
                echo  $this->html;
                break;
            case 3:
                $this->invocarPropuesta3();
                echo  $this->html;
                break;
        }

    }

    private function invocarPropuesta1(){
        require('Propuesta1.php');

        $imprimirNum=new Propuesta1();
        $listadoNumeros = $imprimirNum->imprimirNumeros();
        $this->armarHtml($listadoNumeros);
       
        unset($listadoNumeros);
 
    }

    private function invocarPropuesta2(){
        require('Propuesta2.php');

        $imprimirNum=new Propuesta2();
        $listadoNumeros = $imprimirNum->imprimirNumeros();
        $this->armarHtml($listadoNumeros);
       
        unset($listadoNumeros);
    }

    private function invocarPropuesta3(){
        require('Propuesta3.php');

        $imprimirNum=new Propuesta3();
        $listadoNumeros = $imprimirNum->imprimirNumeros();
        $this->armarHtml($listadoNumeros);
       
        unset($listadoNumeros);
    }

    private function armarHtml($listadoNumeros){
        $this->html .= '<tbody>';
           
        foreach($listadoNumeros as $index => $valor){
            $this->html .='<tr><th scope="row">'.$index.'</th><td>'.$valor.'</td></tr>';
        }

        $this->html .= '</tbody></table>';
    }

}

$imprimirNumeros=new InvocacionesPropuestas();
$imprimirNumeros->invocar($_POST['propuesta']);


