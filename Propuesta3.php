<?php

Class Propuesta3{
	
	private $imprimir=array();

	function __construct(){
		array_push($this->imprimir,"Imprimir números del 1 al 100");
	}
	
	function obtenerResultado($n){
		$this->combinacion1($n);
		$this->combinacion2($n+5);
		$this->combinacion3($n+10);
		
		  
	}  

	function combinacion1($n){
		array_push($this->imprimir,$n+0,$n+1,'Linio',$n+3,'IT');
	}

	function combinacion2($n){
		array_push($this->imprimir,'Linio',$n+1,$n+2,'Linio','IT');
	}

	function combinacion3($n){
		array_push($this->imprimir,$n+0,'Linio',$n+2,$n+3,'Linianos');
	}
	
	
	function imprimirNumeros(){

		for($x=1;$x<=90;$x+=15){
			$this->obtenerResultado($x);
		}
		$this->combinacion1(91);
		$this->combinacion2(96);
		
		return $this->imprimir;
		
	}

	
}
